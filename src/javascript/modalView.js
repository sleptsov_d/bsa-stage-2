import View from './view';
import App from './app';

class ModalView extends View {
    constructor() {
        super();

        this.createModal();
    }

    createModal() {
        const modalOverlay = this.createElement({ tagName: 'div', className: 'modal-overlay', attributes: { id: 'modal' } });
        const modalBox = this.createElement({ tagName: 'div', className: 'modal-box' });
        const modalBoxHeader = this.createElement({ tagName: 'div', className: 'modal-box-header' });
        const modalBoxBody = this.createElement({ tagName: 'div', className: 'modal-box-body' });
        const modalBoxFooter = this.createElement({ tagName: 'div', className: 'modal-box-footer' });
        modalBox.appendChild(modalBoxHeader);
        modalBox.appendChild(modalBoxBody);
        modalBox.appendChild(modalBoxFooter);
        modalOverlay.appendChild(modalBox);
        modalOverlay.addEventListener('click', event => this.hideModal(), false);
        App.rootElement.append(modalOverlay);
    }

    hideModal() {
        const modal = document.getElementById('modal');
        modal.style.visibility = 'hidden';
    }

    static showModal() {
        const modal = document.getElementById('modal');
        modal.style.visibility = 'visible';
    }

}

export default ModalView;