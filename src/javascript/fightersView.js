import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';
import ModalView from './modalView';

class FightersView extends View {
  constructor(fighters) {
    super();

    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
  }

  fightersDetailsMap = new Map();

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }

  async handleFighterClick(event, fighter) {
    let selectedFighter = this.fightersDetailsMap.get(fighter._id);
    if (!selectedFighter) {
      const _fighter = await fighterService.getFighterDetails(fighter._id);
      this.fightersDetailsMap.set(_fighter._id, _fighter);
      selectedFighter = this.fightersDetailsMap.get(_fighter._id);
    }
    console.log('selectedFighter', selectedFighter);
    ModalView.showModal();
    // show modal with fighter info
    // allow to edit health and power in this modal
  }
}

export default FightersView;